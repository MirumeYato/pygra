# PyGra
Python Gravitation N-body simulation  
This is an object oriented software for N-body simulation with Docker support on Python.


## Usage:
Users can run this software as Python JupyterBook in Docker container using Vscode features or just install Python 3 and libraries as usual. More information about how to do it with Docker can be found in article -- https://jessitron.com/2021/09/09/minimum-path-to-a-jupyter-notebook-in-vscode/ . Clone repository and do same stages but using Dockerfile from this project instead. All of your files will be available out of the container and this way just simplifies installation processes. 


## Package structure:
`debug` - directory for debug scripts (where users  can check functionality and make speed tests)
- `acc_calc_lib.py` - is a library for calculation acceleration. There are few versions of this function.Prodactivity of them depends on your hardware. From the speed test by acc_lib_test.py you can search which is better for you.  As default code of examples uses getAcc_numba. At the output dir you can find plots with speed tests.
- `acc_lib_test_hst.py` - test if nupmpy.stack fucntion will be faster.
- `acc_lib_test.py` - python test for `pygra/acc_calc_lib.py`
- `PE_test.py` - python test for potential energy calculations (similar to acc_lib_test.py).
- `test_math_func.c` - C test for `pygra/math_func.c`
- `plots_samples.ipynb` - This file consists of script examples for plots of energy, angular momentum and positions .gif animation. There are no data initialization, so they wouldn't work as JupyterBook, but you can copy samples to your scripts.

`pygra` - directory for any functionality using in code
- `parallel_acc.py` - library with best optimized functions for potential energy and acceleration calculations with package numba for parallelization.
- `G_body.py` - main library with classes for gravity structures at the initial point of time. Helps to generate initial data.
- `G_solver.py` - Declaration script of functions with different methods for solving ODE in N-body issues.
- `G_evolution.py` - main library with classes for gravity structures at any point of time Provides N-body simulation by functions declared in `G_solver.py` and save results for future analysis.
- `math_func.c` - is a library with a math functions writed on C for using it in code.
- `bin` - is folder for compilled files like `lib_math_func.so` (compilled `math_func.c`) 

`output` - directory for your output data. By deafult here you also can find plots pictures from speed test for time simulation - `speed_test_time.png` (`speed_test_time_hstack.png` is similar, but with hstacking results) and for number of bodies - `speed_test_n_bodies.png`

`scripts` - here stored examples of using this package. This examples are jupyter notebooks with different N-body simulations.
- `Common_example.ipynb` - some basic example of usage G_body class and its methods
- `Cluster_sim.ipynb` - example of creation of class G_cluster and some basic uasge of this class for N-body simulations and plots printing.

`.gitirnore`

`.devcontainer` - package installing instructions for vscode. You can ran package in docker conteiner inside vscode (read more https://jessitron.com/2021/09/09/minimum-path-to-a-jupyter-notebook-in-vscode/).

`Dockerfile` - instruction for Docker to create container with compiled package

`README.md`

