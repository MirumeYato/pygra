# Use an official NVIDIA CUDA runtime image
FROM nvidia/cuda:12.0.0-runtime-ubuntu22.04

# Set a non-interactive mode for APT
ENV DEBIAN_FRONTEND=noninteractive

# Install Python3, pip and git
RUN apt-get update && \
    apt-get install -y python3 python3-pip git && \
    rm -rf /var/lib/apt/lists/*

# Upgrade pip/setuptools first
RUN pip3 install --no-cache-dir --upgrade pip setuptools

# Install PyTorch (GPU version), plus other Python packages if needed
# Note: Ensure the CU version matches the base image's CUDA version
RUN pip3 install --no-cache-dir torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu118

# Copy our Python script into the container
# COPY multiflow.py /app/multiflow.py

# Set the working directory
# WORKDIR /app

# Run the Python script
# CMD ["python3", "multiflow.py"]

RUN pip install jupyter

RUN pip install --no-cache-dir --upgrade pip setuptools

RUN pip install --no-cache-dir tqdm ipython Pillow numba

EXPOSE 8888