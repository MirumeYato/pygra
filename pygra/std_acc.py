from __future__ import annotations

import numpy as np
from typing import Type
import numpy.typing as npt
FloatArray = npt.NDArray[np.float64]

def getAcc_std( pos: FloatArray, mass: FloatArray, G: float, softening: float) -> FloatArray:
    """
    Calculate the acceleration on each particle due to Newton's Law 
	pos  is an N x 3 matrix of positions
	mass is an N x 1 vector of masses
	G is Newton's Gravitational constant
	softening is the softening length
	a is N x 3 matrix of accelerations
    """
    # positions r = [x,y,z] for all particles N 
    x: FloatArray = pos[:, 0:1]
    y: FloatArray = pos[:, 1:2]
    z: FloatArray = pos[:, 2:3]

    # matrix that stores all pairwise particle separations: r_j - r_i
    dx: FloatArray = x.T - x # Optimiztion: such operation optimazed very well
    dy: FloatArray = y.T - y
    dz: FloatArray = z.T - z

    # matrix that stores 1/r^3 for all particle pairwise particle separations
    #inv_r3 = ne.evaluate("dx*dx + dy*dy + dz*dz + softening*softening") # softening - for halo particles
    inv_r3: FloatArray = dx * dx + dy * dy + dz * dz + softening * softening # softening - for halo particles
    #check your distance parameter (if it is too big, than there would be calculation problems)

    #inv_r3_coal=inv_r3[inv_r3==softening*softening]
    inv_r3_pos: FloatArray = inv_r3[inv_r3 > 0.]
    # Optimiztion: most time spend here.
    inv_r3[inv_r3 > 0.] = np.power(inv_r3_pos, (-1.5))

    ax: FloatArray = G * (dx * inv_r3) @ mass # Optimiztion: most time spend here.
    ay: FloatArray = G * (dy * inv_r3) @ mass
    az: FloatArray = G * (dz * inv_r3) @ mass
    
    # pack together the acceleration components
    # Optimiztion: most time spend here. Its easier not stack like this.
    return np.hstack((ax, ay, az))

# Optimize some calculation and adding type annotations
def getAcc_opt(pos: FloatArray, mass: FloatArray, G: float, softening: float) -> FloatArray:
    n = pos.shape[0]
    # positions r = [x,y,z] for all particles N 
    x: FloatArray = pos[:, 0:1]
    y: FloatArray = pos[:, 1:2]
    z: FloatArray = pos[:, 2:3]

    # matrix that stores all pairwise particle separations: r_j - r_i
    dx: FloatArray = x.T - x
    dy: FloatArray = y.T - y
    dz: FloatArray = z.T - z

    # calculate the softening radius for each particle
    # Optimiztion: easier to operate with matrixes.
    softening_vec: FloatArray = np.full((n,n), softening)

    # matrix that stores 1/r^3 for all particle pairwise particle separations
    inv_r3: FloatArray = (dx**2 + dy**2 + dz**2 + softening_vec**2)**(-1.5)
    #It is better to zero initializate diogonal, but in not really needed 
    # (dx*inv_r3) - will do the same

    # 1.option: inv_r3-=np.diag(inv_r3)*np.diag(np.ones(n))
    # 2.option: inv_r3[dx**2 + dy**2 + dz**2 == 0.]=0.

    # calculate the acceleration components using broadcasting
    # Optimization: better than usual matrix prod.
    ax: FloatArray = G * np.sum(dx * inv_r3 * mass, axis = 1)[:, np.newaxis]
    ay: FloatArray = G * np.sum(dy * inv_r3 * mass, axis = 1)[:, np.newaxis]
    az: FloatArray = G * np.sum(dz * inv_r3 * mass, axis = 1)[:, np.newaxis]

    # pack together the acceleration components

    return np.hstack((ax.reshape(n, 1), ay.reshape(n, 1), az.reshape(n, 1)))

def getPE_std( pos: FloatArray, mass: FloatArray, 
              G: np.float64, softening: np.float64) -> np.float64:
	"""
	Get potential energy (PE) of simulation
	pos is N x 3 matrix of positions
	vel is N x 3 matrix of velocities
	mass is an N x 1 vector of masses
	G is Newton's Gravitational constant
	PE is the potential energy of the system
	"""
	# Potential Energy:

	# positions r = [x,y,z] for all particles
	x: FloatArray = pos[:,0:1]
	y: FloatArray = pos[:,1:2]
	z: FloatArray = pos[:,2:3]

	# matrix that stores all pairwise particle separations: r_j - r_i
	dx: FloatArray = x.T - x
	dy: FloatArray = y.T - y
	dz: FloatArray = z.T - z

	# matrix that stores 1/r for all particle pairwise particle separations 
	inv_r: FloatArray = np.sqrt(dx**2 + dy**2 + dz**2 + softening * softening)
	inv_r[inv_r > 0.] = 1.0 / inv_r[inv_r>0]

	# sum over upper triangle, to count each interaction only once
	return G * np.sum(np.sum(np.triu(-(mass * mass.T) * inv_r, 1)))