#===============================#
#                               #
#             PyGra             #
#                         v1.02 #
#===============================#
#  ::Python N-body Simuation::  #
#===============================#
#                               #
#     ODE solver functions      #
#                               #
#===============================#

from __future__ import annotations

import sys

from typing import Type
from tqdm import trange

import numpy as np
import numba as nb
from numba import cfunc, njit
from numbalsoda import address_as_void_pointer, dop853, lsoda, lsoda_sig
from scipy.integrate import solve_ivp

from numba import types
from typing import Type
import numpy.typing as npt

# adding project folder to the system path
sys.path.insert(0, '/workspaces/Jupyter_Dockerbook/')
from pygra.parallel_acc import getAcc_numba, getPE_numba

FloatArray = npt.NDArray[np.float64]


#=========================#
#    Numbalsoda solver    #
#=========================#
# Body's ODE - Equation of Motion
def __g_ODE_nb(t, u_, du_,  G, s, arr_mass, n):
    u = nb.carray(u_, (6*n,))
    du = nb.carray(du_, (6*n,))
    acc = getAcc_numba(u[3*n:].reshape(n, 3), arr_mass, G, s) 
    # last parameters is grav. constant and softening
    du[3*n:] = u[:3*n] # dr/dt = vel (where r={x,y,z}, vel={Vx,Vy,Vz})
    du[:3*n:3] = acc[:, 0]  # d(vel_x)/dt = acceleration_x 
    du[1:3*n:3] = acc[:, 1] # d(vel_y)/dt = acceleration_y
    du[2:3*n:3] = acc[:, 2] # d(vel_z)/dt = acceleration_z

# Argument type definition:
## 'G' is gravitational constant
## 's' is softening parameter
## 'arr_mass' is the memory address of array mass
## 'len_arr' is the length of arrray arr_mass
args_dtype = types.Record.make_c_struct([
                    ('G', types.float64),
                    ('s', types.float64),
                    ('arr_p', types.int64),
                    ('len_arr', types.int64)])

# This function will create the numba function to pass to lsoda.
def create_jit_rhs(rhs, args_dtype):
    jitted_rhs = nb.njit(rhs)
    @nb.cfunc(types.void(types.double,
             types.CPointer(types.double),
             types.CPointer(types.double),
             types.CPointer(args_dtype)))
    def wrapped(t, u, du, user_data_p):
        # Unpack G, s and arr_p from user_data_p
        user_data = nb.carray(user_data_p, 1)
        G = user_data[0].G 
        s = user_data[0].s
        arr = nb.carray(address_as_void_pointer(user_data[0].arr_p), (user_data[0].len_arr), dtype = np.float64) # type: ignore
        n = user_data[0].len_arr
        # Then we call the jitted rhs function, passing in data
        jitted_rhs(t, u, du, G, s, arr, n)  # type: ignore
    return wrapped
# Create the function to be called by lsoda
g_ODE_nb = create_jit_rhs(__g_ODE_nb, args_dtype)

# Numbalsoda solver call function
def g_solver_nbl(_method : str, mass: FloatArray, pos: FloatArray, vel: FloatArray, t_eval: FloatArray, G: np.float64,
                  softening: np.float64, _rtol: float = float(1e-15), _atol: float = float(1e-15)) -> tuple[FloatArray, FloatArray]:
    """ 
    This function initializate ODE solver for bodies with [mass,pos,vel]. Time limit is N*dt and dt is step. 

    Parameters:
    - _method: Name of ode method. Numbalsoda have  'dop853' and 'lsoda' methods.
    - _rtol: accurasity of solution. 
    - _atol: accurasity of solution. 
    
    Returns:
    - [pos(t), vel(t)] for each t in range (0.,N*dt,N)
    """

    n : int = mass.shape[0]
    __mass : FloatArray = np.ascontiguousarray(np.array(mass, dtype = np.float64).flatten())
    # initial conditions of pos and vel
    u0 : FloatArray = np.concatenate((vel.reshape(3*n), pos.reshape(3*n)))
    # data you want to pass to ODE (args == G,s,arr_mass,n in the g_ODE_nb)
    args = np.array((np.float64(G), np.float64(softening), __mass.ctypes.data, n), dtype = args_dtype)
    funcptr = g_ODE_nb.address  # address to ODE function
    N = t_eval.shape[0]  # number of times to evaluate solution
    # integrate with f (lsoda or dop853) method
    if _method == "lsoda":    usol, success = lsoda(funcptr, u0, t_eval, data = args, rtol = _rtol, atol = _atol)
    else :  usol, success = dop853(funcptr, u0, t_eval, data = args, rtol = _rtol, atol = _atol)
    return usol[:, 3*n:].reshape(N, n, 3), usol[:, 0:3*n].reshape(N, n, 3)

#=======================#
#      SciPy solver     #
#=======================#
# this method is too slow, but should be very clear 
# Define the system of differential equations

def g_ODE_scipy(t, u, G, softening, mass, n):
    """Definition of the differential equations system."""
    acc = getAcc_numba(u[3*n:].reshape(n, 3), mass, G, softening)
    # last parameters is G,softening
    # For this release it is a constant   
    dx = u[:3*n] # dr/dt = vel (where r={x,y,z}, vel={Vx,Vy,Vz})
    dv = np.empty(3*n, dtype = np.float64)
    dv[:3*n:3] = acc[:, 0]   # d(vel_x)/dt = acceleration_x 
    dv[1:3*n:3] = acc[:, 1]  # d(vel_y)/dt = acceleration_y 
    dv[2:3*n:3] = acc[:, 2]  # d(vel_z)/dt = acceleration_z 
    return  np.array([dv, dx]).flatten() 

def g_solver_scp(_method : str, mass: FloatArray, pos: FloatArray, vel: FloatArray, t_eval: FloatArray, max_dt: np.float64,
                  G: np.float64, softening: np.float64, _rtol: float = float(1e-13), _atol: float = float(1e-13)):
    """ 
    Parameters:
    - _method: methods  Explicit: 'DOP853','RK23', 'RK45', 'LSODA'. Implicit : 'BDF', 'Radau'. More info https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html#scipy.integrate.solve_ivp
    - _rtol: accurasity of solution. 
    - _atol: accurasity of solution. 
    """
    # Set the initial conditions
    n : int = mass.shape[0]
    t_s = (t_eval[0], t_eval[-1])
    u0 : FloatArray = np.concatenate((vel.reshape(n * 3), pos.reshape(n * 3)))
    # Set the parameters
    params = (G, softening, mass.flatten(), n)
    # Solve the system of differential equations using solve_ivp
    solution = solve_ivp(g_ODE_scipy, t_span = t_s, y0 = u0, args = params, max_step = max_dt, method = _method, rtol = _rtol, atol = _atol)
    return solution 

"""  ==== Custom solvers ==== """
 
#=======================#
#          RK4          #
#=======================#
@njit(nb.float64[:,:,:](
        nb.float64, nb.float64[:], nb.float64[:, :], nb.float64[:, :], nb.float64, nb.float64), parallel = True)
def rk4(dt, mass, pos, vel, G, softening):
    '''
    Given a vector y with [x,xdot], calculate the new y for step dt,
    using rk4 method
    '''
    k1_dx, k1_dv = dt * np.stack((vel, getAcc_numba(pos, mass, G, softening)))
    k2_dx, k2_dv = dt * np.stack((vel + 0.5 * k1_dv, getAcc_numba(pos + 0.5 * k1_dx, mass, G, softening)))
    k3_dx, k3_dv = dt * np.stack((vel + 0.5 * k2_dv, getAcc_numba(pos + 0.5 * k2_dx, mass, G, softening)))
    k4_dx, k4_dv = dt * np.stack((vel + k3_dv, getAcc_numba(pos + k3_dx, mass, G, softening)))
    return np.stack((pos + (1/6.) * (k1_dx + 2. * k2_dx + 2. * k3_dx + k4_dx), vel + (1/6.) * (k1_dv + 2. * k2_dv + 2. * k3_dv + k4_dv)))

def g_solver_nbrk(mass: FloatArray, pos: FloatArray, vel: FloatArray, t_eval: FloatArray, 
                 G: np.float64, softening: np.float64) -> tuple[FloatArray, FloatArray]:
    N : int = t_eval.shape[0]
    n : int = mass.shape[0]
    __mass : FloatArray = mass.flatten()
    pos_save : FloatArray = np.empty((N, n, 3), dtype = np.float64)
    vel_save : FloatArray = np.empty((N, n, 3), dtype = np.float64)
    pos_save[0, :, :] = pos
    vel_save[0, :, :] = vel
    __pos : FloatArray = pos.copy()
    __vel : FloatArray = vel.copy()
    # Simulation Main Loop
    for i in trange(1, N):
        dt : np.float64 = t_eval[i] - t_eval[i-1]
        __pos, __vel = rk4(dt, __mass, __pos, __vel, G, softening)
        # save positions
        pos_save[i, :, :] = __pos
        vel_save[i, :, :] = __vel
    return pos_save, vel_save

#=======================#
#         Euler         #
#=======================#
def g_solver_nbe(mass: FloatArray, pos: FloatArray, vel: FloatArray, t_eval: FloatArray, 
                 G: np.float64, softening: np.float64) -> tuple[FloatArray, FloatArray]:
    N : int = t_eval.shape[0]
    n : int = mass.shape[0]
    __mass : FloatArray = mass.flatten()
    pos_save : FloatArray = np.empty((N, n, 3), dtype = np.float64)
    vel_save : FloatArray = np.empty((N, n, 3), dtype = np.float64)
    pos_save[0, :, :] = pos
    vel_save[0, :, :] = vel
    __pos : FloatArray = pos.copy()
    __vel : FloatArray = vel.copy()
    # Simulation Main Loop
    for i in trange(1, N):
        dt : np.float64 = t_eval[i] - t_eval[i-1]
        #
        # Classikal RK1 is:
        # __vel += dt * getAcc_numba(__pos, __mass, G, softening)
        # __pos += __vel * dt
        #
        # A little modification is:
        # (1/2) kick
        __vel += 0.5 * dt * getAcc_numba(__pos, __mass, G, softening)
        # drift
        __pos += __vel * dt
        # update accelerations
        # (1/2) kick
        __vel += 0.5 * dt * getAcc_numba(__pos, __mass, G, softening)
        # save positions
        pos_save[i :, :] = __pos
        vel_save[i :, :] = __vel
    return pos_save, vel_save