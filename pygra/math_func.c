#include <stdlib.h>
#include <stdio.h>

double** m_dist(double* x, int n) {
    double** dist_matrix = malloc(n * sizeof(double*));
    for (int i = 0; i < n; i++) {
        dist_matrix[i] = malloc(n * sizeof(double));
    }
    for (int i = 0; i < n; i++) {
        dist_matrix[i][i] = 0.0;
        for (int j = i + 1; j < n; j++) {
            dist_matrix[i][j] = x[j] - x[i];
            dist_matrix[j][i] = -dist_matrix[i][j];
        }
    }
    return dist_matrix;
}