#===============================#
#                               #
#             PyGra             #
#                         v1.02 #
#================================
#  ::Python N-body Simuation::  #
#===============================#
#                               #
# Evolution class for           #
# simulation dynamic of Bodies. #
#                               #
#===============================#

from __future__ import annotations
import sys
import os

import numpy as np
from typing import Type
from typing import Optional
import numpy.typing as npt

from tqdm import trange
from time import sleep
import h5py
import datetime
from matplotlib import pyplot as plt
from os import listdir
from os.path import isfile, join

# adding Folder_2/subfolder to the system path
sys.path.insert(0, '/workspaces/Jupyter_Dockerbook/')
from pygra.G_body import *
from pygra.G_solver import *


FloatArray = npt.NDArray[np.float64]


def Evolution_help() -> None:
    """ This is short introduction function into G_evolution """
    print("""
    Welcome to N-body simulation pakage PyGra!
    ...

    First of all you need to create G_body or class that inherits it.
    You can show at the '/pygra/scripts' examples of cration such objects.

    Secondly you need to set up every time point until last for simulation
    (recommended time delta is not bigger than 10**7 sec)

    Than you should use one of solution methods with method 'solve'
    ... 
    """)
    pass

def file_naming(start_yr : float = 0, special_name : str = "") -> str:
    """
    Creates a unique file name for output files. 
    By default it will save data to path from 'G_evolution.directiry'
    with name shape gsim_{}kyr (if special_name = "").
    In else cases set your file name by this value

    Parameters:
    - special_name (str): An optional string to custom name creation.
    - start_kyr (float): Start time for this simulation file in [kyr] ({}kyr)

    Returns:
    - file_name (str): A string representing the unique file name.
    """
    # create a unique file name
    if special_name != "" : file_name = special_name
    else: file_name = ('gsim_{:05.0f}yr.h5'.format(start_yr/10**7))
    # print the file name
    return file_name

def project_naming(special_name : str = "") -> str:
    """
    Creates a unique project name for output directory. 
    By default it will save data to path from 'G_evolution.directiry'
    with name shape gsim_{current date/time} (if special_name = "").
    In else cases set your project name by this value

    Parameters:
    - special_name (str): An optional string to custom name creation.
    ({}) - date parameter

    Returns:
    - dir_name (str): A string representing the unique directory name.
    """
    # get the current date and time
    now = datetime.datetime.now()
    # create a unique file name
    if special_name != "" : dir_name = special_name
    else: dir_name = '../output/__gsim_{}'.format(now.strftime('%y%m%d_%H(%M)'))
    # print the file name
    return dir_name

def save_data(file_name : str, mass : FloatArray, pos_t : FloatArray, vel_t : FloatArray, time : FloatArray) -> None:
    """
    Saves mass, position, and velocity data to an HDF5 file(s). 
    If time range is more than (5 * 10**5) elements ,
    than new file, started from (5 * 10**5 + 1) will be created.
    Until the end of data simulation new filed will be created at the same scheme. 

    Parameters:
    - file_name (str): The path+name of the file to be created.
    - mass (np.ndarray): A 1D NumPy array of mass values for each object.
    - pos_t (np.ndarray): A 3D NumPy array of position values for each object at each time step.
    - vel_t (np.ndarray): A 3D NumPy array of velocity values for each object at each time step.
    """
    # Get number of time steps and number of bodies
    N, n, k = pos_t.shape
    # debug: print(N,n,k)
    with h5py.File(file_name, 'w') as f:
        # create a group for the positions
        pos_group = f.create_group('positions')
        # create a dataset for the positions
        pos_dataset = pos_group.create_dataset('data', shape = (N, n, 3), 
                dtype = np.float64, compression = 'gzip', compression_opts = 9)
        # write the positions to the dataset
        pos_dataset[:] = pos_t

        # create a group for the velocities
        vel_group = f.create_group('velocities')
        # create a dataset for the velocities
        vel_dataset = vel_group.create_dataset('data', shape = (N, n, 3), 
                dtype = np.float64, compression = 'gzip', compression_opts = 9)
        # write the velocities to the dataset
        vel_dataset[:] = vel_t

        # create a group for the masses
        mass_group = f.create_group('masses')
        # create a dataset for the masses
        mass_dataset = mass_group.create_dataset('data', shape = (n, 1),  
                dtype = np.float64, compression = 'gzip', compression_opts = 9)
        # write the masses to the dataset
        mass_dataset[:] = mass

        # create a group for the masses
        time_group = f.create_group('time')
        # create a dataset for the masses
        time_dataset = time_group.create_dataset('data', shape = (N),  
                dtype = np.float64, compression = 'gzip', compression_opts = 9)
        # write the masses to the dataset
        time_dataset[:] = time
        print ("Debug: Data saved in " + file_name + " successfully")
        pass

def get_data(path : str, first_item : Optional[int] = None, last_item : Optional[int] = None, 
             step : Optional[int] = None) -> tuple[FloatArray, FloatArray, FloatArray, FloatArray]:
    """
    Opens the HDF5 file with mass, position, velocity and time data.

    Parameters:
    - path (str): The path to the file(s) (or file) to be readed.
    - step (int): If file is too big, than recommended to skip some data.

    Returns:
    - mass_array (np.ndarray): A 1D NumPy array of mass values for each object.
    - positions_array (np.ndarray): A 3D NumPy array of position values for each object at each time step.
    - velocities_array (np.ndarray): A 3D NumPy array of velocity values for each object at each time step.
    - time_array (np.ndarray): A 1D NumPy array of time fot every body at sertain index.
    """
    positions_array : FloatArray = np.empty(0, dtype = np.float64)
    velocities_array : FloatArray = np.empty(0, dtype = np.float64)
    mass_array : FloatArray = np.empty(0, dtype = np.float64)
    time_array : FloatArray = np.empty(0, dtype = np.float64)
    # File case
    if isfile(path) and path.endswith(".h5"): 
        with h5py.File(path, 'r') as f:
            # read the positions dataset
            positions_array = f['positions/data'][first_item:last_item:step, :, :] # type: ignore
            # read the velocities dataset
            velocities_array = f['velocities/data'][first_item:last_item:step, :, :] # type: ignore
            # read the masses dataset
            mass_array = f['masses/data'][:] # type: ignore
            # read the time dataset
            time_array = f['time/data'][first_item:last_item:step] # type: ignore
    # Directory case
    else:
        print ("Error: path is not file '.h5'")
    return mass_array, positions_array, velocities_array, time_array

def get_data_from_dir(path : str, shape_obj : tuple[int, int], 
                      step_size : int = 10) -> tuple[FloatArray, FloatArray, FloatArray, FloatArray]:
    """
    Opens the directory with HDF5 files with mass, position, velocity and time data.
    Use this function when data of your simulation is determined by time in case of operational memory usage. 
    Recommended to use big step size or  get_data function with certain first-last items.

    Parameters:
    - path (str): The path to the file(s) (or file) to be readed.
    - shape_obj (tuple([int],[int]))
    - step (int): If file is too big, than recommended to skip some data.

    Returns:
    - mass_array (np.ndarray): A 1D NumPy array of mass values for each object.
    - positions_array (np.ndarray): A 3D NumPy array of position values for each object at each time step.
    - velocities_array (np.ndarray): A 3D NumPy array of velocity values for each object at each time step.
    - time_array (np.ndarray): A 1D NumPy array of time fot every body at sertain index.
    """
    n_t, n_b = shape_obj
    if step_size > 1: n_t = int(np.floor((n_t + 1) / step_size))
    # we need only files with .h5 format
    files_arr = [x for x in os.listdir(path) if x.endswith(".h5")] 
    # Calculate shorted (correleted to step size) lenth of loading array.
    N_iter = int(np.floor(n_t / len(files_arr)))
    # Prepeare empty arrays
    pos : FloatArray = np.empty((n_t, n_b, 3), dtype = np.float64)
    vel : FloatArray = np.empty((n_t, n_b, 3), dtype = np.float64)
    time_arr : FloatArray = np.empty((n_t, ), dtype = np.float64)
    iteration : int = 0
    # Start loading
    for file in files_arr: 
        print('Loading: ' + file)
        # load data
        mass, pos[iteration * N_iter : (iteration + 1) * N_iter, :, :], vel[
            iteration * N_iter : (iteration + 1) * N_iter, :, :], time_arr[
            iteration * N_iter : (iteration + 1) * N_iter] = get_data(path = path+ '/' + file, step = step_size)
        iteration += 1
    return mass, pos, vel, time_arr

class G_evolution(G_bodies):
    def __init__(self, objects : G_bodies, time_arr : FloatArray = np.empty(0, dtype = np.float64), 
                 directory : str = project_naming()) -> None:
        self.__evolution_moments : FloatArray = time_arr
        self.mass : FloatArray = objects.mass
        self.__pos : FloatArray = objects.pos
        self.__vel : FloatArray = objects.vel
        self.__directory : str = directory
        try:
            os.makedirs(self.__directory)
            print("Debug: Directory created successfully! (path is "+self.__directory+")")
        except OSError as error:
            print(f"Error: Directory creation failed: {error}")
        self.__file : str = file_naming(start_yr = 0)
        super().__init__(self.mass, self.__pos, self.__vel)
        self.__G, self.__softening = self.get_G(), self.get_softening()
        pass
    
    def debug_print(self) -> None:
        super().debug_print()
        print(f'evolution moments is {self.__evolution_moments}')
        print(f'N_time {self.__evolution_moments.shape[0]}')
        print(f'file name and path is {self.__directory + self.__file}')
        pass

    def shape(self) -> tuple[int, int]:
        """ Returns (Number of evolution moments, number of bodies) """
        return (self.__evolution_moments.shape[0], self.mass.shape[0])

    def update_file_name(self, start_time : Optional[float] = None, custom_name : str = "") -> None:
        """ update file name """
        if start_time == None: start_time = self.__evolution_moments[0] # ! FIX !
        self.__file = file_naming(start_yr = start_time, special_name = custom_name) # type: ignore
        pass

    def update_dir_name(self, custom_name : str = "") -> None:
        """ update output directory name """
        self.__directory = project_naming(special_name = custom_name)
        pass

    def get_pos(self) -> FloatArray:
        return self.__pos

    def get_vel(self) -> FloatArray:
        return self.__vel
    
    def update_init_conditions(self, obj : G_bodies):
        self.mass = obj.mass
        self.__pos = obj.get_pos()
        self.__vel = obj.get_vel()
        pass

    def get_file(self) -> tuple[str, str]:
        """ Returns file name and path """
        return self.__directory, self.__file

    def set_time_range(self, time_arr : FloatArray) -> None:
        """
        Set massive of compiling time. 
        - time_arr (np.ndarray): Massiv of moments that will be actually calculated (gives impact on accuracy) 
        """
        self.__evolution_moments = time_arr
        self.update_file_name(start_time = self.__evolution_moments[0]) # ! FIX !
        pass

    def solve_nbl(self, method : str = 'dop853', 
                  _rtol: float = float(1e-15), _atol: float = float(1e-15)) -> tuple[FloatArray, FloatArray]:
        """ 
        Predict or simulate positions and velocities at times in 'evolution_moments'. 
        Recommended to use 'solve' method for simulation, as it well integrated at class.
        This method returns only positions and velocities as arrays.
               ...
        """
        return g_solver_nbl(method, self.mass, self.__pos, self.__vel, self.__evolution_moments, self.__G, 
                            self.__softening, _rtol, _atol)
    
    def solve_scipy(self, method : str = '', 
                  _rtol: float = float(1e-15), _atol: float = float(1e-15)):
        """ 
        Predict or simulate positions and velocities at times in 'evolution_moments'. 
        Recommended to use 'solve' method for simulation, as it well integrated at class.
        Scipy gives special instruments for result analysis, here we get from it only standart data.
        Originally this method returns bunch object with the fields defined in documentation:
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html

        We get from this object position and velositios by '.y' method
            vel_evol = result.y[:3*n,:].reshape(3, n, N)
            pos_evol = result.y[3*n:,:].reshape(3, n, N)
            where - '3' is number of axes, 'n' - number of bodies, 'N' - len of 'evolution_moments'
               ...
        """
        max_dt = np.max(self.__evolution_moments[1:] - self.__evolution_moments[:-1])
        sol = g_solver_scp(method, self.mass, self.__pos, self.__vel, self.__evolution_moments, max_dt,
                             self.__G, self.__softening, _rtol, _atol)
        # Update time. Scipy uses dynamical time step.
        self.set_time_range(sol.t)
        # Unify data
        pos_t, vel_t = (
            sol.y[3 * self.shape()[1]:].T.reshape((self.shape()[0], self.shape()[1], 3)), 
            sol.y[:3 * self.shape()[1]].T.reshape((self.shape()[0], self.shape()[1], 3)))
        return pos_t, vel_t 
    
    def solve_rk4(self, method : str = '', 
                  _rtol: float = float(0), _atol: float = float(0)) -> tuple[FloatArray, FloatArray]:
        """ 
        Predict or simulate positions and velocities at times in 'evolution_moments'. 
        Recommended to use 'solve' method for simulation, as it well integrated at class.
        This method returns only positions and velocities as arrays.
               ...
        """
        return g_solver_nbrk(self.mass, self.__pos, self.__vel, self.__evolution_moments, self.__G, self.__softening)
    
    def solve_euler(self, method : str = '', 
                  _rtol: float = float(0), _atol: float = float(0)) -> tuple[FloatArray, FloatArray]:
        """ 
        Predict or simulate positions and velocities at times in 'evolution_moments'. 
        Recommended to use 'solve' method for simulation, as it well integrated at class.
        This method returns only positions and velocities as arrays.
               ...
        """
        return g_solver_nbe(self.mass, self.__pos, self.__vel, self.__evolution_moments, self.__G, self.__softening)        

    def solve(self, method : str = 'numbalsoda', sub_method : str  = 'dop853', _rtol: float = float(1e-15), 
              _atol: float = float(1e-15), max_data_amount : int = 5 * 10**5 + 1) -> None:
        """
        Solves N-body issue using the specified method, or a default method (numbalsoda dop853) if none is provided.

        NOTE:
        For cases when number of time steps (time_arr lenth) is more than 'max_data_amount' (default is 
        5 * 10**5 + 1) steps, than program can reach limitation of memory usage. Thats why all data will 
        be saved into file, deleted from operational memory and new simulation begins from max_data_amount
        step until the end (or again new file will be created).  
        
        Parameters:
        - method (str, optional): The method to use for solving the equation. If None, a default method will be used.
        - sub_method (str, optional): for 'numbalsoda' or 'scipy' methods
        - max_data_amount (int, optional): Max lenth of simulation data array. Note that its true leth is (max_data_amount * 3 (coord) * N_bodies). More onfo in 'NOTE'.
        
        Returns nothing. Create a project at pygra/output/__gsim...
        It's include .h5 files with simulated data.
        """
        # Determine the method to use
        if np.array([method]) == 'numbalsoda':
            if (np.array([sub_method]) == np.array(['dop853', 'lsoda'])).any():
                solve_func = G_evolution.solve_nbl
            else:
                raise ValueError('Error: Invalid sub-method specified for numbalsoda.')
        elif np.array([method]) == 'scipy':
            if (np.array([sub_method]) == np.array(['RK45', 'RK23', 'DOP853', 'LSODA', 'BDF', 'Radau'])).any():
                solve_func = G_evolution.solve_scipy
            else:
                raise ValueError('Error: Invalid sub-method specified for scipy.')
        elif method == 'RK4':
            solve_func = G_evolution.solve_rk4
        elif method == 'Euler':
            solve_func = G_evolution.solve_euler
        else:
            raise ValueError('Error: Invalid method specified.')
        
        #================================#
        # Here possible to make some dinamyc time step correction.
        # For example: If total energy is lost too much in some caltuclated part - than 
        # from the moment right before delete data and solve again with step lower by some times.
        #================================#
        __evolution_moments_True: FloatArray = np.copy(self.__evolution_moments)
        __body_sys_Tue = G_bodies(self.mass, self.__pos, self.__vel)
        N_iter : int = int(np.ceil(self.__evolution_moments.shape[0] / max_data_amount))
        for iteration in trange(N_iter):
            self.set_time_range(__evolution_moments_True[(iteration) * max_data_amount:(iteration + 1) * max_data_amount])
            if self.__evolution_moments.shape[0] < 2: 
                print("Debug: last file was not created. Too short lenth of simulation's time_array (less than 2). It doesn't affect much on results.")
                break
            # Start simulation and get data
            pos_t, vel_t = solve_func(self, sub_method, _rtol, _atol)
            # Choose name for a new HDF5 file, create it and save data into it 
            self.update_file_name()
            save_data(self.__directory + '/' + self.__file, self.mass, pos_t, vel_t, self.__evolution_moments)
            # Update IC
            self.update_init_conditions(obj = G_bodies(self.mass, pos_t[-1,:,:], vel_t[-1,:,:])) 
            # finally clear memory
            del pos_t
            del vel_t
        # Set back true time array massive
        self.set_time_range(__evolution_moments_True)
        self.update_init_conditions(obj = __body_sys_Tue) 
        # Future dev: Create a file with simulation results info
        pass