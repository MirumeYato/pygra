#===============================#
#                               #
#             PyGra             #
#                         v1.02 #
#===============================#
#  ::Python N-body Simuation::  #
#===============================#
#                               #
# Bodies classes for            #
# creating IC                   #
#                               #
#===============================#

from __future__ import annotations

import sys

from typing import Type, Union
import numpy as np
import numpy.typing as npt
from tqdm import trange
from time import sleep
from matplotlib import pyplot as plt

# adding Folder_2/subfolder to the system path
sys.path.insert(0, '/workspaces/Jupyter_Dockerbook/')
from pygra.parallel_acc import getPE_numba

FloatArray = npt.NDArray[np.float64]


class G_body:
    """General class for one object"""
    __softening : np.float64 = np.float64(1.e8)
    # Newton's Gravitational Constant # km⋅M⊙−1⋅(km/s)**2
    __G : np.float64 = np.float64(3.086 * 10**13 / 233.0) # approx 4.3009172706*3.086*10**10
    def __init__(self, mass : FloatArray, pos : FloatArray, 
                 vel : FloatArray = np.zeros((1,3), dtype = np.float64)) -> None:        
        # positions in [km]
        self.pos : FloatArray = pos
        # velosities in [km/s]
        self.vel : FloatArray = vel
        # masses in [M⊙]
        self.mass : FloatArray = mass
        pass

    # Add bodies to common class 
    def __add__(self, other : G_body) -> G_bodies:
        new_pos : FloatArray = np.append(self.pos, other.pos, axis = 0)
        new_vel : FloatArray = np.append(self.vel, other.vel, axis = 0)
        new_mass : FloatArray = np.append(self.mass, other.mass, axis = 0)
        return G_bodies(new_mass,new_pos,new_vel)
    
    def set_constants(self, softening : np.float64, G : np.float64 = __G) -> None:
        """
        If you need special parameters change it here. 
        
        G [km⋅M_⊙^-1⋅(km/s)**2] -- is gravitational constant (it will be better not to change it)
        softening [km] -- In this simulation objects goes though another and do not merge (for pg version 1.00).
        So this parameter helps when objects goes too close to each other, so infinity numbers not occured. 
        Usually this number at range of 10**6 - 10**8 km
        """
        self.__G : np.float64 = G
        self.__softening : np.float64 = softening
        pass

    def get_G (self) -> np.float64:
        """ Returns G constant """        
        return self.__G
    
    def get_softening (self) -> np.float64:
        """ Returns meaning of softening """        
        return self.__softening
    
    def get_mass_center(self) -> FloatArray:
        """ for G_body cases returns positin coordinats of this body """
        return self.pos
    
    def get_KE(self) -> FloatArray:
        """
        Get kinetic energy of the body 
        (each body if uses as G_bodies)
        """
        return 0.5 * np.sum(self.mass * self.vel * self.vel, axis = 1)

    # Get potencial of the body (if there is one) on certain distance
    def __get_P(self, dist : FloatArray, soft : np.float64 = __softening) -> float:
        p : float = 0.
        try:
            np.seterr(divide='raise')
            p = (self.__G * self.mass.flatten() / np.sqrt(np.sum((self.pos - dist)
                                                               * (self.pos - dist)) + soft * soft))
        except FloatingPointError:
            #debug exception print
            print("Wrong meaning of the distance")
        finally:
            return p
        
    def get_pos(self) -> FloatArray:
        return self.pos

    def get_vel(self) -> FloatArray:
        return self.vel
    
    def debug_print(self) -> None:
        """ Debug method. Print out properties of created object """
        print("Debug print\nMy properties is:")
        print("positions: ", self.pos)
        print("velocities: ", self.vel)
        print("mass: ", self.mass)
        pass

class G_bodies(G_body):
    """ General class for many objects """
    def __init__(self, mass : FloatArray, pos : FloatArray, vel : FloatArray) -> None:
        super().__init__(mass, pos, vel)
        self.__G : np.float64 = self.get_G()
        self.__softening : np.float64 = self.get_softening()
        pass

    def __len__(self) -> int:
        """ Number of bodies """
        return self.mass.shape[0]
    
    def get_mass_center(self) -> FloatArray:
        """ Returns position of bodies's mass center """
        return np.sum(self.pos * self.mass, axis = 0) / np.sum(self.mass)
    
    def set_system_rest(self) -> None:
        """ Choose your cordinte system as mass center that's moving 
        the same at initial moment """
        V_sys : FloatArray = self.mass * self.vel / np.sum(self.mass)
        self.vel -= V_sys
        pass

    def get_PE(self, soft : np.float64) -> FloatArray:
        """ Get potencial energy of each bodies in system """
        pE = getPE_numba(self.pos, self.mass.flatten(), self.__G, soft)
        return pE

    ## Plotting methods
    def plot_pos(self, flag_2D : Union[str, None] = "All", flag_3D: bool = False) -> None:
        """ Plot for bodies positions """
        if flag_2D not in [None, "All", "xOy", "yOz", "xOz"]:
            raise ValueError(f"Invalid value for flag_2D: {flag_2D}")
        fig = plt.figure()
        fig.set_size_inches(18.5, 10.5)
        if flag_3D:
            ax = fig.add_subplot('3d', 141)
            ax.scatter(self.pos[:,0], self.pos[:,1], self.pos[:,2])
        if flag_2D in [None, "All", "xOy"]:
            ax2 = fig.add_subplot(142).set_title('xOy') # type: ignore
            ax2.plot(self.pos[:,0], self.pos[:,1], ".")
        if flag_2D in [None, "All", "yOz"]:
            ax3 = fig.add_subplot(143,).set_title('yOz') # type: ignore
            ax3.plot(self.pos[:,1], self.pos[:,2], ".")
        if flag_2D in [None, "All", "xOz"]:
            ax4 = fig.add_subplot(144).set_title('xOz') # type: ignore
            ax4.plot(self.pos[:,0], self.pos[:,2], ".")  
        plt.show()    

""" === ::Here you can define your classes using classes above:: === """

class G_cluster(G_bodies):
    """Class for clusters"""
    def __init__(self, mass_cl : np.float64, cluster_center : FloatArray, 
                 bodies_num : int) -> None:
        self.mass_cl : np.float64 = mass_cl
        self.pos_center : FloatArray = cluster_center
        self.mass : FloatArray = np.empty((bodies_num, 1), dtype = np.float64)
        self.pos : FloatArray = np.empty((bodies_num, 3), dtype = np.float64)
        self.vel : FloatArray = np.empty((bodies_num, 3), dtype = np.float64)
        super().__init__(self.mass, self.pos, self.vel)
        self.__G : np.float64 = self.get_G()
        self.__softening : np.float64 = self.get_softening()
        pass

    # Position init
    def set_pos_linar_dist(self, R_min = np.float64(0.), R_max = np.float64(1.)) -> None:
        """ Position coordinat linar distribution of objects in ball-shaped claster """
        n : int = len(self)
        phi : FloatArray = np.random.rand(n) * 2 * np.pi
        r : FloatArray = np.random.uniform(R_min, R_max, n)
        theta : FloatArray = np.arccos(np.random.uniform(-1, 1, n))
        self.pos = np.array([r * np.sin(theta) * np.cos(phi) + self.pos_center[0, 0], 
            r * np.sin(theta) * np.sin(phi) + self.pos_center[0, 1], 
            r * np.cos(theta) + self.pos_center[0, 2]]).T
        pass

    def set_pos_uniform_dist(self, R_min = np.float64(0.), R_max = np.float64(1.), p : FloatArray = np.random.uniform(0, 1, (0, 3))) -> None:
        """ Monte-carlo generator of uniform distributed coordinat 
        positions of objects in ball-shaped claster """
        n : int = len(self)
        ip : FloatArray = np.append(p, np.random.uniform(0, R_max, (n - len(p), 3))
                                     * np.sign(np.random.randn(n - len(p), 3)), axis = 0)
        fp : FloatArray = np.delete(ip, np.where(np.array(np.sum(ip**2, axis = 1) > R_max**2)
                                     + np.array(np.sum(ip**2, axis = 1) < (R_min)**2)), axis = 0)
        if not (len(fp) - n == 0):
            return self.set_pos_uniform_dist(R_min, R_max, fp)
        else: 
            self.pos = fp + self.pos_center
        pass
    
    # Mass init
    def set_mass_uniform_dist(self) -> None:
        """ Set mass of class elements equal (cluster mass / number of bodies) """
        n : int = len(self)
        self.mass = self.mass_cl * np.ones((n, 1)) / n
        pass
    
    # Velocities init
    def set_vel_delta_dist(self) -> None:
        """ Set velocities of bodies. Main idea is choosing velocities propotional G(M_cl-m_i)/R """
        n : int = len(self)
        self.vel = np.random.randn(n, 3)
        # Norm calculation
        Norm = 1 / np.sqrt(np.sum(self.vel**2, axis = 1))
        # Distances array
        distances = np.sqrt(np.sum(self.pos**2, axis = 1))
        distances[distances < 1.* 10**8] = 1. # avoid ZeroDivizion error
        # Calculate Amplitude as G(M_cl-m_i)/R
        Amplitude = np.sqrt((self.mass_cl - self.mass.T) * self.__G / distances)
        # Normalization
        Amplitude = Amplitude * Norm
        # Reshape
        Amplitude = np.ones((n, 3)) * (Amplitude).T
        Amplitude[distances < 1.* 10**8] = np.array([[0., 0., 0.]]) # Declare no movement for bodies in center
        self.vel = self.vel * Amplitude
        pass

    def add_object(self, object : G_body) -> None:
        """ Simply add new object to your class. 
        It is useful if you searchng interractions between custom objects and cluster.
        New object can be G_body, G_bodies, e.t.c. """
        self.mass = np.append(self.mass, object.mass, axis = 0)
        self.pos = np.append(self.pos, object.pos, axis = 0)
        self.vel = np.append(self.vel, object.vel, axis = 0)
        self.mass_cl += np.sum(object.mass)
        pass
    
    def number_in_shell(self, R_cl : np.float64) -> int:
        """ Gives you number of bodies inside ball-shaped area with center in mass center """
        pos_in_shell = np.delete(
            self.pos, np.where(np.sum((self.pos - self.get_mass_center())**2, axis = 1) > (R_cl)**2), axis = 0)
        return len(pos_in_shell)

def get_Time_rel(bodies: G_bodies) -> np.float64:
    """ Calulation approximate of Time of relaxation  """
    # N should be > than 2
    N : int = len(bodies)
    G : np.float64 = bodies.get_G()
    R_cl : np.float64 = 0.5 * max(np.sqrt(np.sum(bodies.pos * bodies.pos, axis = 1)))
    M_cl : np.float64 = np.sum(bodies.mass)
    return N / (25.5 * np.log10(0.5 * N)) * np.sqrt(R_cl**(3) / (G * M_cl))

def get_Vel_orb(bodies_1: G_body, bodies_2: G_body, ecc = np.float64(0.)):
    """
    Calculates velocities in apocenter for elleptic orbit with semi-major axis {sm_axis}  and eccentriciry {ecc}. 
    By default ecc = 0. sm_axis has auto-definition in [pc].

    We imply no bodies size and use only sum of mass for each bodies structures (if there are many) 
    for calculations and distance between bodies.

    We interested to find velocities in center of mass system
    in system with center in one of the bodies formula is:
        v**2 = G * (M1 + M2) * (2 / r - 1 / a), where r - relational distance between bodies
    and a is semi-major axis for orbit in this relative system, a = r_max / (1 + ecc).
    
    We need to change our system to resting system of one body and set here wanted parameters.
    For simlifing we use apocenter moment (as initial), so semi-major will be difened automaticly.
    Then |v1*| = 0, |v2*| = |v1| + |v2|. In mass center system m1* |v1| = m2 * |v2|, so 
    back to this system we get |v1| = |v2*| / (1 + m1/m2) and |v2| = |v2*| / (1 + m2/m1).
    """
    
    distance : np.float64 = np.sqrt(np.sum((bodies_1.get_mass_center() - bodies_2.get_mass_center())**2))
    G : np.float64 = bodies_1.get_G()
    M1 : np.float64 = np.sum(bodies_1.mass)
    M2 : np.float64 = np.sum(bodies_2.mass)
    # We get mass's center positions of each bodies
    # and define distance between them as sm_axis * (1 + ecc) - apocenter 
    # or perecenter if ecc < 0
    sm_axis : np.float64 = distance / (1. + ecc) 
    # We choose bodies_1 as resting object.
    print(f"Debug: Orbit parameters is sm_axis = {sm_axis:.3}")
    print(f"Eccetricity = {ecc:.3}")
    # Orbit velocity vector lenth for bodies_1 is:
    return np.sqrt(G * (M1 + M2) * (2/distance - 1/(sm_axis))) / (M1/M2 + 1), np.sqrt(G * (M1 + M2) * (2/distance - 1/(sm_axis))) / (M2/M1 + 1)
   
def get_Period_orb(bodies_1: G_body, bodies_2: G_body,
                   time_delta: float, ecc: float = 0.) -> float:
    G : np.float64 = bodies_1.get_G()
    M1 : np.float64 = np.sum(bodies_1.mass)
    M2 : np.float64 = np.sum(bodies_2.mass)
    distance : np.float64 = np.sqrt(np.sum(
        (bodies_1.get_mass_center() - bodies_2.get_mass_center())**2))
    # full formula is 2 * np.pi * np.sqrt( sm_axis**3 / (G * (M1 + M2)) ) / time_delta, 
    # where sm_axis is semi-major axis for in resting system for one of the bodies  
    return 2 * np.pi * np.sqrt( (distance/(1. + ecc))**3 / (G * (M1 + M2)) ) / time_delta