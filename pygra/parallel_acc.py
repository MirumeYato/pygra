from __future__ import annotations
import numba as nb
import numpy as np
from numba import njit, prange

from typing import Type
import numpy.typing as npt
FloatArray = npt.NDArray[np.float64]

# The usuall and the speedest way to multitask  
@njit(nb.float64[:, :](
    nb.float64[:, :], nb.float64[:], nb.float64, nb.float64), parallel = True)
def getAcc_numba(pos: FloatArray, mass: FloatArray, G: np.float64, 
                 softening: np.float64) -> FloatArray:

    n=pos.shape[0]
    # positions r = [x,y,z] for all particles N
    x : FloatArray = pos[:, 0]
    y : FloatArray = pos[:, 1]
    z : FloatArray = pos[:, 2]
    # accelerarion a = [ax,ay,az] init for all particles N x N
    ax : FloatArray = np.empty((n, 1), dtype = np.float64)
    ay : FloatArray = np.empty((n, 1), dtype = np.float64)
    az : FloatArray = np.empty((n, 1), dtype = np.float64)
    # distsnces dpos = [dx,dy,dz] init for all particles N x N
    dx : FloatArray = np.empty((n, n), dtype = np.float64)
    dy : FloatArray = np.empty((n, n), dtype = np.float64)
    dz : FloatArray = np.empty((n, n), dtype = np.float64)
    # inverse distsnces init N x N
    inv_r3 : FloatArray = np.empty((n, n), dtype = np.float64)
    # calculate the softening radius
    softening_vec : FloatArray = np.full(n, softening)
    m_ones : FloatArray = np.ones(n)

    # main paralleled loop
    for i in prange(n):
        # matrix that stores all pairwise particle separations: r_j - r_i
        dx[i, :] = x - x[i] * m_ones
        dy[i, :] = y - y[i] * m_ones
        dz[i, :] = z - z[i] * m_ones

        # matrix that stores 1/r^3 for all particle pairwise particle separations
        inv_r3[i, :] = np.sqrt(dx[i, :]**2 + dy[i, :]**2 + dz[i, :]**2 + softening_vec**2)**(-3)
        # Diogonal init
        inv_r3[i, i] = 0.

        # calculate the acceleration components using broadcasting and parallelization
        ax[i] = G * np.sum(dx[i,:] * inv_r3[i,:] * mass)
        ay[i] = G * np.sum(dy[i,:] * inv_r3[i,:] * mass)
        az[i] = G * np.sum(dz[i,:] * inv_r3[i,:] * mass)

    return np.hstack((ax.reshape(n, 1), ay.reshape(n, 1), az.reshape(n, 1)))


# The usuall and the speedest way to multitask  
@njit(nb.float64[:](
    nb.float64[:, :], nb.float64[:], nb.float64, nb.float64), parallel = True)
def getPE_numba(pos: FloatArray, mass: FloatArray, G: np.float64, 
                softening: np.float64) -> FloatArray:

    n = pos.shape[0]
    # positions r = [x,y,z] for all particles N
    x : FloatArray = pos[:, 0]
    y : FloatArray = pos[:, 1]
    z : FloatArray = pos[:, 2]
    # distsnces dpos = [dx,dy,dz] init for all particles N x N
    dx : FloatArray = np.empty((n, n), dtype = np.float64)
    dy : FloatArray = np.empty((n, n), dtype = np.float64)
    dz : FloatArray = np.empty((n, n), dtype = np.float64)
    # calculate the softening radius
    softening_vec : FloatArray = np.full(n, softening)
    m_ones : FloatArray = np.ones(n)
    # accelerarion a = [ax,ay,az] init for all particles N x N
    PE : FloatArray = np.empty(n, dtype = np.float64)
    # inverse distsnces init N x N
    inv_r3 : FloatArray = np.empty((n, n), dtype = np.float64)
    # calculate the softening radius
    softening_vec : FloatArray = np.full(n, softening)

    # main paralleled loop
    for i in prange(n):
        # matrix that stores all pairwise particle separations: r_j - r_i
        dx[i, :] = x - x[i] * m_ones
        dy[i, :] = y - y[i] * m_ones
        dz[i, :] = z - z[i] * m_ones
        
        # matrix that stores 1/r^3 for all particle pairwise particle separations
        inv_r3[i, :] = 1 / np.sqrt((dx[i, :]**2 + dy[i, :]**2 + dz[i, :]**2 + softening_vec**2))
        # Diogonal init
        inv_r3[i, i] = 0.

        # calculate the acceleration components using broadcasting and parallelization
        PE[i] = G * np.sum(mass[i] * inv_r3[i, :] * mass)

    return PE