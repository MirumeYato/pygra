from __future__ import annotations

import numpy as np
import numba as nb
from numba import njit, prange

from typing import Type, Union
import numpy.typing as npt
from tqdm import trange
from time import sleep

FloatArray = npt.NDArray[np.float64]
""" 
    This is library for some acceleration calculation.
    There are presented different ways to make such functions
    and some time test script. 
"""

#standart acceleration function
def getAcc_std( pos, mass, G, softening ):
    """
    Calculate the acceleration on each particle due to Newton's Law 
	pos  is an N x 3 matrix of positions
	mass is an N x 1 vector of masses
	G is Newton's Gravitational constant
	softening is the softening length
	a is N x 3 matrix of accelerations
    """
    # positions r = [x,y,z] for all particles N 
    x = pos[:,0:1]
    y = pos[:,1:2]
    z = pos[:,2:3]

    # matrix that stores all pairwise particle separations: r_j - r_i
    dx = x.T - x # Optimiztion: such operation optimazed very well
    dy = y.T - y
    dz = z.T - z

    # matrix that stores 1/r^3 for all particle pairwise particle separations
    #inv_r3 = ne.evaluate("dx*dx + dy*dy + dz*dz + softening*softening") # softening - for halo particles
    inv_r3 = dx*dx + dy*dy + dz*dz + softening*softening # softening - for halo particles
    #check your distance parameter (if it is too big, than there would be calculation problems)

    #inv_r3_coal=inv_r3[inv_r3==softening*softening]
    inv_r3_pos=inv_r3[inv_r3>0.]
    # Optimiztion: most time spend here.
    inv_r3[inv_r3>0.] = np.power(inv_r3_pos,(-1.5))

    ax = G * (dx * inv_r3) @ mass # Optimiztion: most time spend here.
    ay = G * (dy * inv_r3) @ mass
    az = G * (dz * inv_r3) @ mass
    
    # pack together the acceleration components
    # Optimiztion: most time spend here. Its easier not stack like this.
    a = np.hstack((ax,ay,az))

    return a

# Optimize some calculation and adding type annotations
def getAcc_opt(pos: FloatArray, mass: FloatArray, G: float, softening: float) -> FloatArray:
    n=pos.shape[0]
    # positions r = [x,y,z] for all particles N 
    x: FloatArray = pos[:, 0:1]
    y: FloatArray = pos[:, 1:2]
    z: FloatArray = pos[:, 2:3]

    # matrix that stores all pairwise particle separations: r_j - r_i
    dx: FloatArray = x.T - x
    dy: FloatArray = y.T - y
    dz: FloatArray = z.T - z

    # calculate the softening radius for each particle
    # Optimiztion: easier to operate with matrixes.
    softening_vec: FloatArray = np.full((n,n), softening)

    # matrix that stores 1/r^3 for all particle pairwise particle separations
    inv_r3: FloatArray = (dx**2 + dy**2 + dz**2 + softening_vec**2)**(-1.5)
    #It is better to zero initializate diogonal, but in not really needed 
    # (dx*inv_r3) - will do the same

    # 1.option: inv_r3-=np.diag(inv_r3)*np.diag(np.ones(n))
    # 2.option: inv_r3[dx**2 + dy**2 + dz**2 == 0.]=0.

    # calculate the acceleration components using broadcasting
    # Optimization: better than usual matrix prod.
    ax: FloatArray = G * np.sum(dx * inv_r3 * mass, axis = 1)[:, np.newaxis]
    ay: FloatArray = G * np.sum(dy * inv_r3 * mass, axis = 1)[:, np.newaxis]
    az: FloatArray = G * np.sum(dz * inv_r3 * mass, axis = 1)[:, np.newaxis]

    # pack together the acceleration components
    # Optimiztion: most time spend here. Its easier not stack like this.
    #a: FloatArray = np.hstack((ax, ay, az))

    return ax, ay, az

""" multitasc function optimization """

# The usuall way to multitask with using symmetrization  
@njit(nb.types.Tuple((nb.float64[:], nb.float64[:], nb.float64[:]))(
    nb.float64[:, :], nb.float64[:], nb.float64, nb.float64), parallel=True)
def getAcc_nb_sym(pos: nb.float64[:, :], mass: nb.float64[:], G: nb.float64, 
softening: nb.float64) -> nb.types.Tuple((nb.float64[:, :], nb.float64[:, :], nb.float64[:, :])):

    n=pos.shape[0]
    # positions r = [x,y,z] for all particles N 
    x : nb.float64[:] = pos[:, 0]
    y : nb.float64[:] = pos[:, 1]
    z : nb.float64[:] = pos[:, 2]

    # accelerarion a = [ax,ay,az] init for all particles N x N
    ax : nb.float64[:] = np.empty((n), dtype = np.float64)
    ay : nb.float64[:] = np.empty((n), dtype = np.float64)
    az : nb.float64[:] = np.empty((n), dtype = np.float64)

    # distsnces dpos = [dx,dy,dz] init for all particles N x N
    dx : nb.float64[:, :] = np.empty((n, n), dtype = np.float64)
    dy : nb.float64[:, :] = np.empty((n, n), dtype = np.float64)
    dz : nb.float64[:, :] = np.empty((n, n), dtype = np.float64)
    
    # inverse distsnces init N x N
    inv_r3 : nb.float64[:, :] = np.empty((n, n), dtype = np.float64)

    # calculate the softening radius
    softening_vec : nb.float64[:] = np.full(n, softening)
    m_ones : nb.float64[:] = np.ones(n)

    # main paralleled loop
    for i in prange(n-1):
        # matrix that stores all pairwise particle separations: r_j - r_i
        dx[i, i+1:] = x[i+1:] - x[i] * m_ones[i + 1:]
        dy[i, i+1:] = y[i+1:] - y[i] * m_ones[i + 1:]
        dz[i, i+1:] = z[i+1:] - z[i] * m_ones[i + 1:]
        # Symmetrization
        dx[i+1:, i] = -dx[i, i + 1:]
        dy[i+1:, i] = -dy[i, i + 1:]
        dz[i+1:, i] = -dz[i, i + 1:]
        # Diogonal init
        dx[i, i] = 0.
        dy[i, i] = 0.
        dz[i, i] = 0.
        # matrix that stores 1/r^3 for all particle pairwise particle separations
        inv_r3[i, i + 1:] = (dx[i, i + 1:]**2 + dy[i, i + 1:]**2 + dz[i, i + 1:]**2 + softening_vec[i + 1:]**2)**(-1.5)
        # Symmetrization
        inv_r3[i + 1:, i] = inv_r3[i, i + 1:]
        # Diogonal init
        inv_r3[i, i] = 0.
    # Diogonal init for last element
    dx[n-1, n-1] = 0.
    dy[n-1, n-1] = 0.
    dz[n-1, n-1] = 0.
    inv_r3[n-1, n-1] = 0.

    # calculate the acceleration components using matrix prodaction
    ax = G * np.dot((dx * inv_r3), mass)
    ay = G * np.dot((dy * inv_r3), mass)
    az = G * np.dot((dz * inv_r3), mass)

    return ax,ay,az

# The usuall and the speedest way to multitask  
@njit(nb.types.Tuple((nb.float64[:, :], nb.float64[:, :], nb.float64[:, :]))(
    nb.float64[:, :], nb.float64[:], nb.float64, nb.float64), parallel=True)
def getAcc_numba(pos: nb.float64[:, :], mass: nb.float64[:],G: nb.float64, 
softening: nb.float64) -> nb.types.Tuple((nb.float64[:, :], nb.float64[:, :], nb.float64[:, :])):

    n=pos.shape[0]
    # positions r = [x,y,z] for all particles N
    x:nb.float64[:] = pos[:, 0]
    y:nb.float64[:] = pos[:, 1]
    z:nb.float64[:] = pos[:, 2]
    # accelerarion a = [ax,ay,az] init for all particles N x N
    ax:nb.float64[:, :] = np.empty((n, 1), dtype = np.float64)
    ay:nb.float64[:, :] = np.empty((n, 1), dtype = np.float64)
    az:nb.float64[:, :] = np.empty((n, 1), dtype = np.float64)
    # distsnces dpos = [dx,dy,dz] init for all particles N x N
    dx:nb.float64[:, :] = np.empty((n, n), dtype = np.float64)
    dy:nb.float64[:, :] = np.empty((n, n), dtype = np.float64)
    dz:nb.float64[:, :] = np.empty((n, n), dtype = np.float64)
    # inverse distsnces init N x N
    inv_r3:nb.float64[:, :] = np.empty((n, n), dtype = np.float64)
    # calculate the softening radius
    softening_vec:nb.float64[:] = np.full(n, softening)
    m_ones:nb.float64[:] = np.ones(n)

    # main paralleled loop
    for i in prange(n):
        # matrix that stores all pairwise particle separations: r_j - r_i
        dx[i, :] = x - x[i] * m_ones
        dy[i, :] = y - y[i] * m_ones
        dz[i, :] = z - z[i] * m_ones

        # matrix that stores 1/r^3 for all particle pairwise particle separations
        inv_r3[i, :] = (dx[i, :]**2 + dy[i,:]**2 + dz[i, :]**2 + softening_vec**2)**(-1.5)
        # Diogonal init
        inv_r3[i,i]=0.

        # calculate the acceleration components using broadcasting and parallelization
        ax[i] = G * np.sum(dx[i, :] * inv_r3[i, :] * mass)
        ay[i] = G * np.sum(dy[i, :] * inv_r3[i, :] * mass)
        az[i] = G * np.sum(dz[i, :] * inv_r3[i, :] * mass)

    return ax,ay,az

# The optimized way to multitask with using symmetrization 
@njit(nb.types.Tuple((nb.float64[:], nb.float64[:], nb.float64[:]))(
    nb.float64[:, :], nb.float64[:], nb.float64, nb.float64), parallel=True)
def getAcc_nb_sym_opt(pos: nb.float64[:, :], mass: nb.float64[:],G: nb.float64, 
softening: nb.float64) -> nb.types.Tuple((nb.float64[:, :], nb.float64[:, :], nb.float64[:, :])):

    n=pos.shape[0]
    # positions r = [x,y,z] for all particles N
    x:nb.float64[:] = pos[:, 0]
    y:nb.float64[:] = pos[:, 1]
    z:nb.float64[:] = pos[:, 2]
    # accelerarion a = [ax,ay,az] init for all particles N x N
    ax:nb.float64[:] = np.empty((n), dtype = np.float64)
    ay:nb.float64[:] = np.empty((n), dtype = np.float64)
    az:nb.float64[:] = np.empty((n), dtype = np.float64)
    # distsnces dpos = [dx,dy,dz] init for all particles N x N
    dx:nb.float64[:, :] = np.zeros((n, n), dtype = np.float64) # Optimization: 
    dy:nb.float64[:, :] = np.zeros((n, n), dtype = np.float64) # less rows of code, but more time
    dz:nb.float64[:, :] = np.zeros((n, n), dtype = np.float64)
    # inverse distsnces init N x N
    inv_r3:nb.float64[:, :] = np.zeros((n, n), dtype = np.float64)
    # calculate the softening radius
    softening_vec:nb.float64[:] = np.full(n, softening)
    m_ones:nb.float64[:] = np.ones(n)

    # main paralleled loop
    for i in prange(n-1):
        # matrix that stores all pairwise particle separations: r_j - r_i
        dx[i, i + 1:] = x[i + 1:] - x[i] * m_ones[i + 1:]
        dy[i, i + 1:] = y[i + 1:] - y[i] * m_ones[i + 1:]
        dz[i, i + 1:] = z[i + 1:] - z[i] * m_ones[i + 1:]
        # matrix that stores 1/r^3 for all particle pairwise particle separations
        inv_r3[i, i + 1:] = (dx[i, i + 1:]**2 + dy[i, i + 1:]**2 + dz[i, i + 1:]**2 + softening_vec[i + 1:]**2)**(-1.5)
    # Symmetrization
    inv_r3f:nb.float64[:, :] = inv_r3 + inv_r3.T
    # calculate the acceleration components using numpy matrix prod
    ax = G * np.dot(((dx - dx.T) * inv_r3f), mass) # Optimization:
    ay = G * np.dot(((dy - dy.T) * inv_r3f), mass) # dx-dx.T is symmetrization optimization
    az = G * np.dot(((dz - dz.T) * inv_r3f), mass)

    return ax,ay,az

"""
    There are ways to write this function on C or C++ and than call it in python,
    but actually this way is not good. Reason for this is translation of
    data from C to python. Here below is example of usage this method
"""
# Python function for distanve matrix calculation 
# it is mach slower than another ways 
# especially x.T-x is faster in 10 times

# njit and cfunc from numba do not do here any job for speedup
#@cfunc(nb.float64[:,:](nb.float64[:]))

def m_dist(x):
    n = x.shape[0]
    dist_matrix = np.empty((n, n),dtype = np.float64) 
    # initialize distance matrix to a square of zeros
    for i in range(n):
        dist_matrix[i, i] = 0. 
        for j in range(i + 1, n):
            dist_matrix[i, j] = x[j] - x[i] 
            # for the symmetric part, no computation
            dist_matrix[j, i] = -dist_matrix[i, j]
    return dist_matrix

# Fast enough calculation but not faster than usual x.T-x
import ctypes
def m_dist_c(arr = [1.0, 2.0, 3.0]):
    n = arr.shape[0]
    # Load the shared library
    lib = ctypes.CDLL('./lib_math_func.so')

    # Define the argument and return types of the C function
    lib.m_dist.argtypes = [ctypes.POINTER(ctypes.c_double), ctypes.c_int]
    lib.m_dist.restype = ctypes.POINTER(ctypes.POINTER(ctypes.c_double))

    # Call the C function with the appropriate arguments
    c_arr = (ctypes.c_double * n)(*arr)
    result = lib.m_dist(c_arr, n)

    return result

# Transcription data from C to python
# Unfortunately can be optimized by numba    
def c_to_ndarray_std(res_c,n):
    res = np.empty((n, n),dtype = np.float64)
    for i in range(n):
        for j in range(i, n):
            res[i, j] = res_c[i][j]
            res[j, i] = res_c[j][i]
    return res 

# Transcription data from C to python
def c_to_ndarray_short(res_c,n):
    # Convert the result to a Python list
    return np.array([res_c[i][j] for i in range(n) for j in range(n)]).reshape(n, n)

