import sys
# adding Folder_2/subfolder to the system path
sys.path.insert(0, '/workspaces/Jupyter_Dockerbook/')
from debug.acc_calc_lib import getAcc_std, getAcc_opt, getAcc_nb_sym, getAcc_numba, getAcc_nb_sym_opt
import numpy as np
from matplotlib import pyplot as plt

""" Some example of speed test for those functions """


"""
This function give you a posibility to make some speed research
in f - you choose name of funtion:
    {getAcc_std,getAcc_opt,getAcc_numba,
    getAcc_nb_sym, getAcc_nb_sym_opt}
n - number of bodies (int)
n_time - time of calculation (int)
    In N-body simulation you want to see dynamics in time
    and this parameter discribes this time
n_att - affect on approximations of speed time (int)
flag - helps with different structures of getAcc fucntions
    {"std","opt","nb"}
"""
def speed_test_nb(f, n,n_time,n_att,flag):
    # Define input parameters
    G = 1.0
    softening = 0.1
    tot_time=np.empty(n_att,dtype=np.float128)
    for i in range(n_att):
        pos = np.random.rand(n, 3)
        mass = np.random.rand(n,1)
        if flag=="nb" or flag=="opt": 
            # Calculate acceleration using getAcc_nb\opt
            start_time = timeit.default_timer()
            for t in trange(n_time): ax,ay,az = f(pos, mass.T[0], G, softening)
            end_time = timeit.default_timer()
        else:
            # Calculate acceleration using getAcc_std
            start_time = timeit.default_timer()
            for t in trange(n_time): a = f(pos, mass, G, softening)
            end_time = timeit.default_timer()
        tot_time[i] = end_time - start_time
    # You can uncommet lines lower if you want to check reuslts correction 
    # but it will affect on speed test time

    #if flag=="nb": a=np.hstack((ax.reshape(n,1),ay.reshape(n,1),az.reshape(n,1)))
    #elif flag=="opt": a=np.hstack((ax,ay,az))

    return np.append(np.sum(tot_time)/n_att,(np.max(tot_time)-np.min(tot_time))/2)#,a

if __name__ == "__main__":  

    import timeit
    from tqdm import trange

    # Define input parameters
    n=200
    n_time=100
    pos = np.random.rand(n, 3)
    mass = np.random.rand(n,1)
    G = 1.0
    softening = 0.1

    # Calculate acceleration using getAcc_std
    start_time = timeit.default_timer()
    for t in trange(n_time): a_std= getAcc_std(pos, mass, G, softening)
    end_time = timeit.default_timer()
    print("Time for getAcc_std: ", end_time - start_time, " seconds")

    # Calculate acceleration using getAcc_opt
    start_time = timeit.default_timer()
    for t in trange(n_time): ax_opt,ay_opt,az_opt= getAcc_opt(pos, mass.T[0], G, softening)
    end_time = timeit.default_timer()
    print("Time for getAcc_opt: ", end_time - start_time, " seconds")
    a_opt=np.hstack((ax_opt,ay_opt,az_opt))

    # Calculate acceleration using getAcc_nb
    start_time = timeit.default_timer()
    for t in trange(n_time): ax_nb,ay_nb,az_nb = getAcc_numba(pos, mass.T[0], G, softening)
    end_time = timeit.default_timer()
    print("Time for getAcc_nb: ", end_time - start_time, " seconds")
    a_nb=np.hstack((ax_nb.reshape(n,1),ay_nb.reshape(n,1),az_nb.reshape(n,1)))

    # Calculate acceleration using getAcc_nb_sym
    start_time = timeit.default_timer()
    for t in trange(n_time): ax_nb_sym,ay_nb_sym,az_nb_sym = getAcc_nb_sym(pos, mass.T[0], G, softening)
    end_time = timeit.default_timer()
    print("Time for getAcc_nb_sym: ", end_time - start_time, " seconds")
    a_nb_sym=np.hstack((ax_nb_sym.reshape(n,1),ay_nb_sym.reshape(n,1),az_nb_sym.reshape(n,1)))

    # Calculate acceleration using getAcc_nb_sym
    start_time = timeit.default_timer()
    for t in trange(n_time): ax_nb,ay_nb,az_nb = getAcc_numba(pos, mass.T[0], G, softening)
    end_time = timeit.default_timer()
    print("Time for getAcc_nb_sym: ", end_time - start_time, " seconds")
    a_nb=np.hstack((ax_nb.reshape(n,1),ay_nb.reshape(n,1),az_nb.reshape(n,1)))

    # Calculate acceleration using getAcc_nb_sym_opt
    start_time = timeit.default_timer()
    for t in trange(n_time): ax_nb_sym_opt,ay_nb_sym_opt,az_nb_sym_opt = getAcc_nb_sym_opt(pos, mass.T[0], G, softening)
    end_time = timeit.default_timer()
    print("Time for getAcc_nb_sym_opt: ", end_time - start_time, " seconds")
    a_nb_sym_opt=np.hstack((ax_nb_sym_opt.reshape(n,1),ay_nb_sym_opt.reshape(n,1),az_nb_sym_opt.reshape(n,1)))

    #check is result correct
    assert np.allclose(a_std, a_opt)
    assert np.allclose(a_std, a_nb)
    assert np.allclose(a_std, a_nb_sym)
    assert np.allclose(a_std, a_nb_sym_opt)

    """ speed test Plots """

    """ for Time of simulation (N=200) """
    t_arr=np.arange(1,1002,500)
    t_arr2=np.arange(1,5002,500)

    # It will take some time
    x_arr1=np.array([speed_test_nb(getAcc_std, 200,i,3,"std") for i in t_arr])
    x_arr2=np.array([speed_test_nb(getAcc_opt, 200,i,3,"opt") for i in t_arr2])
    x_arr3=np.array([speed_test_nb(getAcc_numba, 200,i,3,"nb") for i in t_arr2])
    x_arr4=np.array([speed_test_nb(getAcc_nb_sym, 200,i,3,"nb") for i in t_arr2])
    x_arr5=np.array([speed_test_nb(getAcc_nb_sym_opt, 200,i,3,"nb") for i in t_arr2])


    plt.plot(t_arr,x_arr1.T[0],label="std")
    plt.plot(t_arr2,x_arr2.T[0],label="opt")
    plt.plot(t_arr2,x_arr3.T[0],label="nb")
    plt.plot(t_arr2,x_arr4.T[0],label="nb_sym")
    plt.plot(t_arr2,x_arr5.T[0],label="nb_sym_opt")
    plt.legend(loc='lower right')
    plt.savefig('../output/speed_test_time.png')
    plt.show()

    """ for Number of bodies test (time of simulation = 1 step) """
    n_arr=np.append(np.arange(2,5003,500),np.array([10000]))

    # It will take some time
    x_arr1=np.array([speed_test_nb(getAcc_std, i,1,3,"std") for i in n_arr])
    x_arr2=np.array([speed_test_nb(getAcc_opt, i,1,3,"opt") for i in n_arr])
    x_arr3=np.array([speed_test_nb(getAcc_numba, i,1,3,"nb") for i in n_arr])
    x_arr4=np.array([speed_test_nb(getAcc_nb_sym, i,1,3,"nb") for i in n_arr])
    x_arr5=np.array([speed_test_nb(getAcc_nb_sym_opt, i,1,3,"nb") for i in n_arr])

    plt.plot(n_arr,x_arr1.T[0],label="std")
    plt.plot(n_arr,x_arr2.T[0],label="opt")
    plt.plot(n_arr,x_arr3.T[0],label="nb")
    plt.plot(n_arr,x_arr4.T[0],label="nb_sym")
    plt.plot(n_arr,x_arr5.T[0],label="nb_sym_opt")
    plt.legend(loc='upper left')
    plt.savefig('../output/speed_test_n_bodies.png')
    plt.show()