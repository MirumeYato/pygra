from __future__ import annotations
""" Some example of speed test for those functions """
# Here we provide similar speed test as in acc_lib_test.py
# but with hstack return
import timeit
from tqdm import trange
import numpy as np
import numba as nb
from numba import njit, prange
from matplotlib import pyplot as plt

from typing import Type
import numpy.typing as npt
FloatArray = npt.NDArray[np.float64]

#standart acceleration function
def getAcc_std( pos, mass, G, softening ):
    """
    Calculate the acceleration on each particle due to Newton's Law 
	pos  is an N x 3 matrix of positions
	mass is an N x 1 vector of masses
	G is Newton's Gravitational constant
	softening is the softening length
	a is N x 3 matrix of accelerations
    """
    # positions r = [x,y,z] for all particles N 
    x = pos[:,0:1]
    y = pos[:,1:2]
    z = pos[:,2:3]

    # matrix that stores all pairwise particle separations: r_j - r_i
    dx = x.T - x # Optimiztion: such operation optimazed very well
    dy = y.T - y
    dz = z.T - z

    # matrix that stores 1/r^3 for all particle pairwise particle separations
    #inv_r3 = ne.evaluate("dx*dx + dy*dy + dz*dz + softening*softening") # softening - for halo particles
    inv_r3 = dx*dx + dy*dy + dz*dz + softening*softening # softening - for halo particles
    #check your distance parameter (if it is too big, than there would be calculation problems)

    #inv_r3_coal=inv_r3[inv_r3==softening*softening]
    inv_r3_pos=inv_r3[inv_r3>0.]
    # Optimiztion: most time spend here.
    inv_r3[inv_r3>0.] = np.power(inv_r3_pos,(-1.5))

    ax = G * (dx * inv_r3) @ mass # Optimiztion: most time spend here.
    ay = G * (dy * inv_r3) @ mass
    az = G * (dz * inv_r3) @ mass
    
    # pack together the acceleration components
    # Optimiztion: most time spend here. Its easier not stack like this.
    a = np.hstack((ax,ay,az))
    return a

# The usuall and the speedest way to multitask  
@njit(nb.float64[:, :](
    nb.float64[:, :], nb.float64[:], nb.float64, nb.float64), parallel=True)
def getAcc_numba(pos: nb.float64[:, :], mass: nb.float64[:],G: nb.float64, 
softening: nb.float64) -> nb.float64[:, :]:

    n=pos.shape[0]
    # positions r = [x,y,z] for all particles N
    x:nb.float64[:] = pos[:, 0]
    y:nb.float64[:] = pos[:, 1]
    z:nb.float64[:] = pos[:, 2]
    # accelerarion a = [ax,ay,az] init for all particles N x N
    ax:nb.float64[:, :] = np.empty((n, 1), dtype=np.float64)
    ay:nb.float64[:, :] = np.empty((n, 1), dtype=np.float64)
    az:nb.float64[:, :] = np.empty((n, 1), dtype=np.float64)
    # distsnces dpos = [dx,dy,dz] init for all particles N x N
    dx:nb.float64[:, :] = np.empty((n, n), dtype=np.float64)
    dy:nb.float64[:, :] = np.empty((n, n), dtype=np.float64)
    dz:nb.float64[:, :] = np.empty((n, n), dtype=np.float64)
    # inverse distsnces init N x N
    inv_r3:nb.float64[:, :] = np.empty((n, n), dtype=np.float64)
    # calculate the softening radius
    softening_vec:nb.float64[:] = np.full(n, softening)
    m_ones:nb.float64[:] = np.ones(n)

    # main paralleled loop
    for i in prange(n):
        # matrix that stores all pairwise particle separations: r_j - r_i
        dx[i,:] = x-x[i]*m_ones
        dy[i,:] = y-y[i]*m_ones
        dz[i,:] = z-z[i]*m_ones

        # matrix that stores 1/r^3 for all particle pairwise particle separations
        inv_r3[i,:] = np.sqrt(dx[i,:]**2 + dy[i,:]**2 + dz[i,:]**2 + softening_vec**2)**(-3)
        # Diogonal init
        inv_r3[i,i]=0.

        # calculate the acceleration components using broadcasting and parallelization
        ax[i] = G * np.sum(dx[i,:] * inv_r3[i,:] * mass)
        ay[i] = G * np.sum(dy[i,:] * inv_r3[i,:] * mass)
        az[i] = G * np.sum(dz[i,:] * inv_r3[i,:] * mass)

    return np.hstack((ax.reshape(n,1),ay.reshape(n,1),az.reshape(n,1)))

# Optimize some calculation and adding type annotations
def getAcc_opt(pos: FloatArray, mass: FloatArray, G: float, softening: float) -> FloatArray:
    n=pos.shape[0]
    # positions r = [x,y,z] for all particles N 
    x: FloatArray = pos[:, 0:1]
    y: FloatArray = pos[:, 1:2]
    z: FloatArray = pos[:, 2:3]

    # matrix that stores all pairwise particle separations: r_j - r_i
    dx: FloatArray = x.T - x
    dy: FloatArray = y.T - y
    dz: FloatArray = z.T - z

    # calculate the softening radius for each particle
    # Optimiztion: easier to operate with matrixes.
    softening_vec: FloatArray = np.full((n,n), softening)

    # matrix that stores 1/r^3 for all particle pairwise particle separations
    inv_r3: FloatArray = (dx**2 + dy**2 + dz**2 + softening_vec**2)**(-1.5)
    #It is better to zero initializate diogonal, but in not really needed 
    # (dx*inv_r3) - will do the same

    # 1.option: inv_r3-=np.diag(inv_r3)*np.diag(np.ones(n))
    # 2.option: inv_r3[dx**2 + dy**2 + dz**2 == 0.]=0.

    # calculate the acceleration components using broadcasting
    # Optimization: better than usual matrix prod.
    ax: FloatArray = G * np.sum(dx * inv_r3 * mass, axis=1)[:, np.newaxis]
    ay: FloatArray = G * np.sum(dy * inv_r3 * mass, axis=1)[:, np.newaxis]
    az: FloatArray = G * np.sum(dz * inv_r3 * mass, axis=1)[:, np.newaxis]

    # pack together the acceleration components
    # Optimiztion: most time spend here. Its easier not stack like this.
    #a: FloatArray = np.hstack((ax, ay, az))

    return np.hstack((ax.reshape(n,1),ay.reshape(n,1),az.reshape(n,1)))
    
def speed_test_nb(f, n,n_time,n_att,flag):
    # Define input parameters
    G = 1.0
    softening = 0.1
    tot_time=np.empty(n_att,dtype=np.float128)
    for i in range(n_att):
        pos = np.random.rand(n, 3)
        mass = np.random.rand(n,1)
        if flag=="nb" or flag=="opt": 
            # Calculate acceleration using getAcc_nb\opt
            start_time = timeit.default_timer()
            for t in trange(n_time): a = f(pos, mass.T[0], G, softening)
            end_time = timeit.default_timer()
        else:
            # Calculate acceleration using getAcc_std
            start_time = timeit.default_timer()
            for t in trange(n_time): a = f(pos, mass, G, softening)
            end_time = timeit.default_timer()
        tot_time[i] = end_time - start_time
    # You can uncommet lines lower if you want to check reuslts correction 
    # but it will affect on speed test time

    #if flag=="nb": a=np.hstack((ax.reshape(n,1),ay.reshape(n,1),az.reshape(n,1)))
    #elif flag=="opt": a=np.hstack((ax,ay,az))

    return np.append(np.sum(tot_time)/n_att,(np.max(tot_time)-np.min(tot_time))/2)#,a

if __name__ == "__main__":
    # Define input parameters
    n=200
    n_time=100
    pos = np.random.rand(n, 3)
    mass = np.random.rand(n,1)
    G = 1.0
    softening = 0.1
    # Calculate acceleration using getAcc_std
    start_time = timeit.default_timer()
    for t in trange(n_time): a_std= getAcc_std(pos, mass, G, softening)
    end_time = timeit.default_timer()
    print("Time for getAcc_std: ", end_time - start_time, " seconds")

    # Calculate acceleration using getAcc_opt
    start_time = timeit.default_timer()
    for t in trange(n_time): a_opt= getAcc_opt(pos, mass.T[0], G, softening)
    end_time = timeit.default_timer()
    print("Time for getAcc_opt: ", end_time - start_time, " seconds")

    # Calculate acceleration using getAcc_nb
    start_time = timeit.default_timer()
    for t in trange(n_time): a_nb = getAcc_numba(pos, mass.T[0], G, softening)
    end_time = timeit.default_timer()
    print("Time for getAcc_nb: ", end_time - start_time, " seconds")

    #check is result correct
    assert np.allclose(a_std, a_opt)
    assert np.allclose(a_std, a_nb)
        
    """ speed test Plots """

    """ for Time of simulation (N=200) """
    t_arr=np.arange(1,1002,500)
    t_arr2=np.arange(1,5002,500)

    # It will take some time
    x_arr1=np.array([speed_test_nb(getAcc_std, 200,i,3,"std") for i in t_arr])
    x_arr2=np.array([speed_test_nb(getAcc_opt, 200,i,3,"opt") for i in t_arr2])
    x_arr3=np.array([speed_test_nb(getAcc_numba, 200,i,3,"nb") for i in t_arr2])

    
    plt.plot(t_arr,x_arr1.T[0],label="std")
    plt.plot(t_arr2,x_arr2.T[0],label="opt")
    plt.plot(t_arr2,x_arr3.T[0],label="nb")
    plt.legend(loc='lower right')
    plt.savefig('../output/speed_test_time_hstack.png')
    plt.show()