""" 
This script provide similar to acc_lib_test.py speed test, 
but for potential energy
"""
import sys
sys.path.insert(0, '/workspaces/Jupyter_Dockerbook/')
from pygra.std_acc import getPE_std

import numpy as np
import numba as nb
from numba import njit, prange

# The usuall and the speedest way to multitask  
@njit(nb.float64[:](
    nb.float64[:, :],nb.float64[:, :],nb.float64[:, :], nb.float64[:], nb.float64, nb.float64), parallel=True)
def getPE_numba(dx: nb.float64[:, :],dy: nb.float64[:, :],dz: nb.float64[:, :], mass: nb.float64[:],G: nb.float64, 
softening: nb.float64) -> nb.float64[:]:

    n=dx.shape[0]
    # accelerarion a = [ax,ay,az] init for all particles N x N
    PE:nb.float64[:] = np.empty(n, dtype=np.float64)
    # inverse distsnces init N x N
    inv_r3:nb.float64[:, :] = np.empty((n, n), dtype=np.float64)
    # calculate the softening radius
    softening_vec:nb.float64[:] = np.full(n, softening)

    # main paralleled loop
    for i in prange(n):
        # matrix that stores 1/r^3 for all particle pairwise particle separations
        inv_r3[i,:] = (dx[i,:]**2 + dy[i,:]**2 + dz[i,:]**2 + softening_vec**2)**(-0.5)
        # Diogonal init
        inv_r3[i,i]=0.

        # calculate the acceleration components using broadcasting and parallelization
        PE[i] = G * np.sum(mass[i]*inv_r3[i,:] * mass)

    return PE

# The usuall and the speedest way to multitask  
@njit(nb.float64[:](
    nb.float64[:, :],nb.float64[:, :],nb.float64[:, :], nb.float64[:], nb.float64, nb.float64), parallel=True)
def getPE_nb_np(dx: nb.float64[:, :],dy: nb.float64[:, :],dz: nb.float64[:, :], mass: nb.float64[:],G: nb.float64, 
softening: nb.float64) -> nb.float64[:]:

    n=dx.shape[0]
    # accelerarion a = [ax,ay,az] init for all particles N x N
    PE:nb.float64[:] = np.empty(n, dtype=np.float64)
    # inverse distsnces init N x N
    inv_r3:nb.float64[:, :] = np.empty((n, n), dtype=np.float64)
    # calculate the softening radius
    softening_vec:nb.float64[:] = np.full(n, softening)

    # main paralleled loop
    for i in prange(n):
        # matrix that stores 1/r^3 for all particle pairwise particle separations
        inv_r3[i,:] = np.sqrt((dx[i,:]**2 + dy[i,:]**2 + dz[i,:]**2 + softening_vec**2))**(-1)
        # Diogonal init
        inv_r3[i,i]=0.

        # calculate the acceleration components using broadcasting and parallelization
        PE[i] = G * np.sum(mass[i]*inv_r3[i,:] * mass)

    return PE

# The usuall and the speedest way to multitask  
@njit(nb.float64[:](
    nb.float64[:, :],nb.float64[:, :],nb.float64[:, :], nb.float64[:], nb.float64, nb.float64), parallel=True)
def getPE_nb_sq(dx: nb.float64[:, :],dy: nb.float64[:, :],dz: nb.float64[:, :], mass: nb.float64[:],G: nb.float64, 
softening: nb.float64) -> nb.float64[:]:

    n=dx.shape[0]
    # accelerarion a = [ax,ay,az] init for all particles N x N
    PE:nb.float64[:] = np.empty(n, dtype=np.float64)
    # inverse distsnces init N x N
    inv_r3:nb.float64[:, :] = np.empty((n, n), dtype=np.float64)
    # calculate the softening radius
    softening_vec:nb.float64[:] = np.full(n, softening)

    # main paralleled loop
    for i in prange(n):
        # matrix that stores 1/r^3 for all particle pairwise particle separations
        inv_r3[i,:] = 1/np.sqrt((dx[i,:]**2 + dy[i,:]**2 + dz[i,:]**2 + softening_vec**2))
        # Diogonal init
        inv_r3[i,i]=0.

        # calculate the acceleration components using broadcasting and parallelization
        PE[i] = G * np.sum(mass[i]*inv_r3[i,:] * mass)

    return PE


# The usuall and the speedest way to multitask  
@njit(nb.float64[:](
    nb.float64[:, :], nb.float64[:], nb.float64, nb.float64), parallel=True)
def getPE_nbb(pos: nb.float64[:, :], mass: nb.float64[:],G: nb.float64, 
softening: nb.float64) -> nb.float64[:]:

    n=pos.shape[0]
    # positions r = [x,y,z] for all particles N
    x:nb.float64[:] = pos[:, 0]
    y:nb.float64[:] = pos[:, 1]
    z:nb.float64[:] = pos[:, 2]
    # distsnces dpos = [dx,dy,dz] init for all particles N x N
    dx:nb.float64[:, :] = np.empty((n, n), dtype=np.float64)
    dy:nb.float64[:, :] = np.empty((n, n), dtype=np.float64)
    dz:nb.float64[:, :] = np.empty((n, n), dtype=np.float64)
    # calculate the softening radius
    softening_vec:nb.float64[:] = np.full(n, softening)
    m_ones:nb.float64[:] = np.ones(n)
    # accelerarion a = [ax,ay,az] init for all particles N x N
    PE:nb.float64[:] = np.empty(n, dtype=np.float64)
    # inverse distsnces init N x N
    inv_r3:nb.float64[:, :] = np.empty((n, n), dtype=np.float64)
    # calculate the softening radius
    softening_vec:nb.float64[:] = np.full(n, softening)

    # main paralleled loop
    for i in prange(n):
        # matrix that stores all pairwise particle separations: r_j - r_i
        dx[i,:] = x-x[i]*m_ones
        dy[i,:] = y-y[i]*m_ones
        dz[i,:] = z-z[i]*m_ones
        
        # matrix that stores 1/r^3 for all particle pairwise particle separations
        inv_r3[i,:] = 1/np.sqrt((dx[i,:]**2 + dy[i,:]**2 + dz[i,:]**2 + softening_vec**2))
        # Diogonal init
        inv_r3[i,i]=0.

        # calculate the acceleration components using broadcasting and parallelization
        PE[i] = G * np.sum(mass[i]*inv_r3[i,:] * mass)

    return PE

if __name__ == "__main__":
    import timeit
    from tqdm import trange

    # Define input parameters
    n=500
    n_time=500
    pos = np.random.rand(n, 3)
    mass = np.random.rand(n,1)
    G = 1.0
    softening = 0.1



    # Calculate acceleration using getPE_nb_np
    start_time = timeit.default_timer()
    for t in trange(n_time): 
        x = pos[:,0:1]
        y = pos[:,1:2]
        z = pos[:,2:3]
        # matrix that stores all pairwise particle separations: r_j - r_i
        dx = x.T - x
        dy = y.T - y
        dz = z.T - z
        a_nb2= getPE_nb_np(dx,dy,dz, mass.T[0], G, softening)
    end_time = timeit.default_timer()
    print("Time for getPE_nb_np: ", end_time - start_time, " seconds")

    # Calculate acceleration using getPE_nb_sq
    start_time = timeit.default_timer()
    for t in trange(n_time):     
        x = pos[:,0:1]
        y = pos[:,1:2]
        z = pos[:,2:3]
        # matrix that stores all pairwise particle separations: r_j - r_i
        dx = x.T - x
        dy = y.T - y
        dz = z.T - z
        a_nb3= getPE_nb_sq(dx,dy,dz, mass.T[0], G, softening)
    end_time = timeit.default_timer()
    print("Time for getPE_nb_sq: ", end_time - start_time, " seconds")

    # Calculate acceleration using getPE_std
    start_time = timeit.default_timer()
    for t in trange(n_time): a_std = getPE_std(pos, mass, G)
    end_time = timeit.default_timer()
    print("Time for getPE_std: ", end_time - start_time, " seconds")

    # Calculate acceleration using getPE_nbb
    start_time = timeit.default_timer()
    for t in trange(n_time): a_nb = getPE_nbb(pos, mass.T[0], G, softening)
    end_time = timeit.default_timer()
    print("Time for getPE_nbb: ", end_time - start_time, " seconds")

    assert np.allclose(-a_std,np.sum(a_nb)/2)
    assert np.allclose(-a_std,np.sum(a_nb2)/2)
    assert np.allclose(-a_std,np.sum(a_nb3)/2)