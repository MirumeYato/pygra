"""
    This script provide speed test for different ways
    of massive addition. 

    There thre ways of addition :
     - numpy addition
     - multitask:
        - by coordinate 
        - by number of bodies
"""
import numpy as np
import numba as nb
from numba import njit, prange

# Addinion by number of bodies
@njit(nb.float64[:, :](
    nb.float64[:, :], nb.float64[:, :]), parallel=True)
def add_p_n(p1,p2):
    n,m=p1.shape
    p=np.empty((n,m),dtype=np.float64)
    for i in prange(n):
        p[i,:]=p1[i,:]+p2[i,:]
    return p

# Addinion by number coordinates
@njit(nb.float64[:, :](
    nb.float64[:, :], nb.float64[:, :]), parallel=True)
def add_p_m(p1,p2):
    n,m=p1.shape
    p=np.empty((n,m),dtype=np.float64)
    for i in prange(m):
        p[:,i]=p1[:,i]+p2[:,i]
    return p

# Multitask function for adding x,y,z to p1[x,y,z]
@njit(nb.float64[:, :](
    nb.float64[:, :], nb.float64[:], nb.float64[:], nb.float64[:]), parallel=True)
def add_p_nm(p1,x,y,z):
    n,m=p1.shape
    p=np.empty((n,m),dtype=np.float64)
    for i in prange(n):
        p[i,0]=p1[i,0]+x[i]
        p[i,1]=p1[i,1]+y[i]
        p[i,2]=p1[i,2]+z[i]
    return p

if __name__ == "__main__":  
    import timeit
    from tqdm import trange

    # Define input parameters
    n=100
    n_time=100
    pos = np.random.rand(n, 3)
    #mass = np.random.rand(n,1)
    G = 1.0
    softening = 0.1

    # Numpy Addinion
    start_time = timeit.default_timer()
    for t in trange(n_time): pos1= pos+pos
    end_time = timeit.default_timer()
    print("Time for numpy: ", end_time - start_time, " seconds")

    # Addinion by number coordinates
    start_time = timeit.default_timer()
    for t in trange(n_time): pos2=add_p_m(pos,pos)
    end_time = timeit.default_timer()
    print("Time for m case: ", end_time - start_time, " seconds")

    # Addinion by number of bodies
    start_time = timeit.default_timer()
    for t in trange(n_time): pos3=add_p_n(pos,pos)
    end_time = timeit.default_timer()
    print("Time for n case: ", end_time - start_time, " seconds")

    # Check equal
    assert np.allclose(pos1, pos2)
    assert np.allclose(pos1, pos3)

    """
    For tested hardware in most cases numby is better in one order of magnitude
    """

    """
    Cause we have an tulpe of np.arrays at the result of getAcc_nb function,
    than we intrested to test the case if we will reshape this result in 
    common vecotor or add by coordinates 
    """
    # Define input parameters
    n=10000
    n_time=10000
    p0 = np.random.rand(n,3)
    x = np.random.rand(n)
    y = np.random.rand(n)
    z = np.random.rand(n)
    #mass = np.random.rand(n,1)
    G = 1.0
    softening = 0.1

    # Numpy Addinion with reshape of x,y,z results of getAcc-nb functions
    start_time = timeit.default_timer()
    for t in trange(n_time): 
        pos=np.hstack((x.reshape(n,1),y.reshape(n,1),z.reshape(n,1)))
        pos1= p0+pos
    end_time = timeit.default_timer()
    print("Time for numpy: ", end_time - start_time, " seconds")

    # Addinion with miltitask by number of bodies
    start_time = timeit.default_timer()
    for t in trange(n_time): 
        pos2= add_p_nm(p0,x,y,z)
    end_time = timeit.default_timer()
    print("Time for numpy: ", end_time - start_time, " seconds")

    assert np.allclose(pos1, pos2)

    """
    For tested hardware in most cases numby is better in two times
    But when we use N approximately equal 10^(4) and bigger - 
    add_p_nm becomes faster than usual numpy methods
    """

    pass
