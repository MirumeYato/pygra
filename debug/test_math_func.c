#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../pygra/math_func.c"



int main() {
    double x[] = {1.0, 2.0, 3.0, 4.0, 5.0};
    const int n = 5;
        
    clock_t start = clock(); // start measuring time
    
    double** dist_matrix = m_dist(x, n);
    
    clock_t end = clock(); // end measuring time
    double time_spent = (double)(end - start) / CLOCKS_PER_SEC; // calculate time difference
    
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            printf("%lf ", dist_matrix[i][j]);
        }
        printf("\n");
    }
    
    printf("Elapsed time: %lf seconds\n", time_spent); // print elapsed time
    return 0;
}
